import pickle
from core.calculator import Calculator
from core.api import Features, ScoringResult, ScoringDecision


class SimpleModel(object):
    """Класс для моделей c расчетом proba и threshold."""

    _threshold = 0.3
    _approved_amount = 100_000

    def __init__(self, model_path: str):
        """Создает объект класса."""
        with open(model_path, 'rb') as pickled_model:
            self._model = pickle.load(pickled_model)

    def get_scoring_result(self, features: Features) -> ScoringResult:
        """Возвращает объект ScoringResult с результатами скоринга."""
        proba = self._predict_proba(features)

        decision = ScoringDecision.DECLINED
        amount = 0
        if proba < self._threshold:
            amount = self._approved_amount
            decision = ScoringDecision.ACCEPTED

        return ScoringResult(
            decision=decision,
            amount=amount,
            threshold=self._threshold,
            proba=proba,
        )

    def _predict_proba(self, features: Features) -> float:
        """Определяет вероятность невозврата займа."""
        # важен порядок признаков для catboost
        return self._model.predict_proba(
            features.income_category,
            features.age_category,
            features.credit_bureau_category,
        )


class AdvancedModel(SimpleModel):

    def __init__(self, model_path: str):
        super().__init__(model_path)
        self._calculator = Calculator()

    def get_scoring_result(self, features: Features) -> ScoringResult:
        """Возвращает объект ScoringResult с результатами скоринга."""
        proba = self._predict_proba(features)

        decision = ScoringDecision.DECLINED
        amount = 0
        if proba < 0.3:
            decision = ScoringDecision.ACCEPTED
            amount = self._calculator.calc_amount(
                proba,
                features,
            )

        return ScoringResult(
            decision=decision,
            amount=amount,
            threshold=self._threshold,
            proba=proba,
        )
